package com.saka.excelreader.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.saka.excelreader.models.Application;
import com.saka.excelreader.models.Feature;
import com.saka.excelreader.models.Product;
import com.saka.excelreader.models.ProductCategory;
import com.saka.excelreader.models.UIProductByIdModel;
import com.saka.excelreader.models.UIProductCategoryModel;
import com.saka.excelreader.models.UIProductModel;
import com.saka.excelreader.repositories.ProductCatagoryRepo;
import com.saka.excelreader.repositories.ProductRepo;

@RestController
@RequestMapping("/")
public class ProductController 
{
	@Autowired
	ProductRepo productRepo;
	
	@Autowired
	ProductCatagoryRepo productCatagoryRepo;
	
	@RequestMapping(value="products", method = RequestMethod.GET)
	public List<UIProductModel> getProductList()
	{
		List<UIProductModel> uiProductModelList = new ArrayList<UIProductModel>();
		
		List<Product> product = productRepo.findAll();
		Iterator<Product> itr = product.iterator();
		while(itr.hasNext())
		{
			Product p=itr.next();
			UIProductModel uiProductModel = new UIProductModel();
			uiProductModel.setId(p.getId());
			uiProductModel.setName(p.getName());
			uiProductModel.setDescription(p.getDescription());
			uiProductModel.setProductCategory((p.getProductCategory()).getCatagoryName());
			uiProductModel.setUrl(p.getUrl());
			uiProductModelList.add(uiProductModel);
		}
		return uiProductModelList;
	}
	
	
	@RequestMapping(value="productscategories", method=RequestMethod.GET)
	public List<UIProductCategoryModel> getProductCategoriesList()
	{
		List<UIProductCategoryModel> uiProductCategoryModelList = new ArrayList<UIProductCategoryModel>();
		List<ProductCategory> productCategories = productCatagoryRepo.findAll();
		Iterator<ProductCategory> itr = productCategories.iterator();
		while(itr.hasNext())
		{
			ProductCategory pc = itr.next();
			UIProductCategoryModel uiProductCategoryModel =new UIProductCategoryModel();
			uiProductCategoryModel.setId(pc.getCategoryId());
			uiProductCategoryModel.setName(pc.getCatagoryName());
			uiProductCategoryModel.setDescription(pc.getDescription());
			uiProductCategoryModelList.add(uiProductCategoryModel);
		}
		return uiProductCategoryModelList;
	}
	
	@RequestMapping(value="products/{id}")
	public UIProductByIdModel getProductsById(@PathVariable(value="id") Long id)
	{
		Optional<Product> product = productRepo.findById(id);
		UIProductByIdModel uiProductByIdModel = new UIProductByIdModel();
		Product p1=product.get();
		
		if(p1!=null)
		{
			uiProductByIdModel.setName(p1.getName());
			uiProductByIdModel.setDescription(p1.getDescription());
			uiProductByIdModel.setImageURL(p1.getUrl());
			ProductCategory pc = p1.getProductCategory();
			
			//Category details of the given product ID
			LinkedHashMap<String, Object> productCategoryDetails = setProductCategoryDetailsInMap(pc);
			uiProductByIdModel.setProductCategory(productCategoryDetails);
			
			
			//Finding products from other category
			List<Product> products = productRepo.findByproductCategory(p1.getProductCategory());
			List<UIProductModel> otherProductFromSameCategoryList = new ArrayList<UIProductModel>();
			Iterator<Product> itr = products.iterator();
			while(itr.hasNext())
			{
				Product p=itr.next();
				UIProductModel uiProductModel = new UIProductModel();
				uiProductModel.setId(p.getId());
				uiProductModel.setName(p.getName());
				uiProductModel.setDescription(p.getDescription());
				uiProductModel.setProductCategory((p.getProductCategory()).getCatagoryName());
				uiProductModel.setUrl(p.getUrl());
				otherProductFromSameCategoryList.add(uiProductModel);
			}
			uiProductByIdModel.setOtherProductsFromSameCatagory(otherProductFromSameCategoryList);
		}
        return uiProductByIdModel;      
	}
	
	
	public LinkedHashMap<String, Object> setProductCategoryDetailsInMap(ProductCategory pc)
	{
		LinkedHashMap<String, Object> productCategoryDetails = new LinkedHashMap<String,Object>();
		productCategoryDetails.put("CategoryName ", pc.getCatagoryName());
		productCategoryDetails.put("Catagorydesc", pc.getDescription());
		
		List<Application> application = pc.getApplications();
		List<String> applicationList = new ArrayList<String>();
		Iterator<Application> itr1 = application.iterator();
		while(itr1.hasNext())
		{
			applicationList.add((itr1.next()).getDescription());
		}
		productCategoryDetails.put("Applications", applicationList);
		
		List<Feature> features = pc.getFeatures();
		List<String> featureList = new ArrayList<String>();
		Iterator<Feature> itr2 = features.iterator();
		while(itr2.hasNext())
		{
			featureList.add((itr2.next()).getDescription());
		}
		
		productCategoryDetails.put("Features", featureList);
		return productCategoryDetails;
	}
	
}
