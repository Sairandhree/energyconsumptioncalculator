package com.saka.excelreader.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.saka.excelreader.models.Product;
import com.saka.excelreader.models.ProductCategory;

public interface ProductRepo extends JpaRepository<Product, Long> 
{
	public Optional<Product> findById(Long id);
	
	public List<Product> findByproductCategory(ProductCategory category);
	/*public static final String FIND_PRODUCTS = "SELECT ID, Category, description, Name, url FROM product";

	@Query(value = FIND_PRODUCTS, nativeQuery = true)
	public List<Product> findByProducts();
*/	
	//List<Product> getByIdAndName();
}
