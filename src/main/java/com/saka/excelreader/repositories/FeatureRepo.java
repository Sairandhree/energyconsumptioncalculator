package com.saka.excelreader.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.saka.excelreader.models.Feature;


public interface FeatureRepo extends JpaRepository<Feature, Long> {

}
