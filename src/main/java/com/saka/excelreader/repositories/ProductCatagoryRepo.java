package com.saka.excelreader.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.saka.excelreader.models.Product;
import com.saka.excelreader.models.ProductCategory;

public interface ProductCatagoryRepo extends JpaRepository<ProductCategory, Long>
{
	//public Optional<ProductCategory> findById(Long id);
}
