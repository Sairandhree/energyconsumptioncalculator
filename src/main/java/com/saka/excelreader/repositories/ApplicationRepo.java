package com.saka.excelreader.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.saka.excelreader.models.Application;


public interface ApplicationRepo extends JpaRepository<Application, Long>{

}
