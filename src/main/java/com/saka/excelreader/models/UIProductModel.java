package com.saka.excelreader.models;

public class UIProductModel 
{
	private Long id;
	private String productCategory;
	private String name;
	private String description;
	private String url;
	
	//private LinkedHashMap<String, Object> productDetails;
	/*private String automaticGrade;
	private String brand;
	private String capacity;
	
	//todo - make it enum
	private String design;
	
	//todo make it enum
	private String driven;
	private String material;
	private int minOrderQuantity;*/

	public String getProductCategory() {
		return productCategory;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

/*	public LinkedHashMap<String, Object> getProductDetails() {
		return productDetails;
	}

	public void setProductDetails(LinkedHashMap<String, Object> productDetails) {
		this.productDetails = productDetails;
	}
	*/
	
}
