package com.saka.excelreader.models;

public class OutputModel {

	private double fuelCompsiontion;
	private double fuelCompsiontionPerKGProduct;
	private double fuelCompsiontionPerKGEvaporation;
	private double fuelCost;
	private double fuelCostPerKGProduct;
	private double fuelCostPerKGEvaporation;
	public double getFuelCompsiontion() {
		return fuelCompsiontion;
	}
	public void setFuelCompsiontion(double fuelCompsiontion) {
		this.fuelCompsiontion = fuelCompsiontion;
	}
	public double getFuelCompsiontionPerKGProduct() {
		return fuelCompsiontionPerKGProduct;
	}
	public void setFuelCompsiontionPerKGProduct(double fuelCompsiontionPerKGProduct) {
		this.fuelCompsiontionPerKGProduct = fuelCompsiontionPerKGProduct;
	}
	public double getFuelCompsiontionPerKGEvaporation() {
		return fuelCompsiontionPerKGEvaporation;
	}
	public void setFuelCompsiontionPerKGEvaporation(double fuelCompsiontionPerKGEvaporation) {
		this.fuelCompsiontionPerKGEvaporation = fuelCompsiontionPerKGEvaporation;
	}
	public double getFuelCost() {
		return fuelCost;
	}
	public void setFuelCost(double fuelCost) {
		this.fuelCost = fuelCost;
	}
	public double getFuelCostPerKGProduct() {
		return fuelCostPerKGProduct;
	}
	public void setFuelCostPerKGProduct(double fuelCostPerKGProduct) {
		this.fuelCostPerKGProduct = fuelCostPerKGProduct;
	}
	public double getFuelCostPerKGEvaporation() {
		return fuelCostPerKGEvaporation;
	}
	public void setFuelCostPerKGEvaporation(double fuelCostPerKGEvaporation) {
		this.fuelCostPerKGEvaporation = fuelCostPerKGEvaporation;
	}
	public OutputModel(double fuelCompsiontion, double fuelCompsiontionPerKGProduct,
			double fuelCompsiontionPerKGEvaporation, double fuelCost, double fuelCostPerKGProduct,
			double fuelCostPerKGEvaporation) {
		super();
		this.fuelCompsiontion = fuelCompsiontion;
		this.fuelCompsiontionPerKGProduct = fuelCompsiontionPerKGProduct;
		this.fuelCompsiontionPerKGEvaporation = fuelCompsiontionPerKGEvaporation;
		this.fuelCost = fuelCost;
		this.fuelCostPerKGProduct = fuelCostPerKGProduct;
		this.fuelCostPerKGEvaporation = fuelCostPerKGEvaporation;
	}
	@Override
	public String toString() {
		return "OutputModel [fuelCompsiontion=" + fuelCompsiontion + ", fuelCompsiontionPerKGProduct="
				+ fuelCompsiontionPerKGProduct + ", fuelCompsiontionPerKGEvaporation="
				+ fuelCompsiontionPerKGEvaporation + ", fuelCost=" + fuelCost + ", fuelCostPerKGProduct="
				+ fuelCostPerKGProduct + ", fuelCostPerKGEvaporation=" + fuelCostPerKGEvaporation + "]";
	}
	public OutputModel() {
		
	}
	
	
	
	
}
