package com.saka.excelreader.models;


import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Product {
	
	@Id
	@GeneratedValue
	private Long id;
	
	//todo - make it enum
	private String automaticGrade;
	private String brand;
	private String capacity;
	
	@ManyToOne
	@JoinColumn(name="category_id")
	private ProductCategory productCategory ;
	private String description;
	//todo - make it enum
	private String design;
	
	//todo make it enum
	private String driven;
	private String material;
	private int minOrderQuantity;
	
	private String name;
	private String url;
	
	
	private Product() {
		super();
	}


	public Product( String automaticGrade, String brand, String capacity, ProductCategory productCategory,
			String description, String design, String driven, String material, int minOrderQuantity, String name,
			String url) {
		super();
		//this.id = id;
		this.automaticGrade = automaticGrade;
		this.brand = brand;
		this.capacity = capacity;
		this.productCategory = productCategory;
		this.description = description;
		this.design = design;
		this.driven = driven;
		this.material = material;
		this.minOrderQuantity = minOrderQuantity;
		this.name = name;
		this.url = url;
	}


	public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getAutomaticGrade() {
		return automaticGrade;
	}


	public void setAutomaticGrade(String automaticGrade) {
		this.automaticGrade = automaticGrade;
	}


	public String getBrand() {
		return brand;
	}


	public void setBrand(String brand) {
		this.brand = brand;
	}


	public String getCapacity() {
		return capacity;
	}


	public void setCapacity(String capacity) {
		this.capacity = capacity;
	}


	public ProductCategory getProductCategory() {
		return productCategory;
	}


	public void setProductCategory(ProductCategory productCategory) {
		this.productCategory = productCategory;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getDesign() {
		return design;
	}


	public void setDesign(String design) {
		this.design = design;
	}


	public String getDriven() {
		return driven;
	}


	public void setDriven(String driven) {
		this.driven = driven;
	}


	public String getMaterial() {
		return material;
	}


	public void setMaterial(String material) {
		this.material = material;
	}


	public int getMinOrderQuantity() {
		return minOrderQuantity;
	}


	public void setMinOrderQuantity(int minOrderQuantity) {
		this.minOrderQuantity = minOrderQuantity;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getUrl() {
		return url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	@Override
	public String toString() {
		return "Product [id=" + id + ", automaticGrade=" + automaticGrade + ", brand=" + brand + ", capacity="
				+ capacity + ", productCategory=" + productCategory + ", description=" + description + ", design="
				+ design + ", driven=" + driven + ", material=" + material + ", minOrderQuantity=" + minOrderQuantity
				+ ", name=" + name + ", url=" + url + "]";
	}
	
		
}
