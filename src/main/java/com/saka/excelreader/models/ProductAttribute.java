package com.saka.excelreader.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class ProductAttribute {
	
	@Id
	@GeneratedValue
	private Long id;
	
	private String description;
	
	

	public ProductAttribute() {
		super();
		
	}

	public ProductAttribute(Long id, String description) {
		super();
		this.id = id;
		this.description = description;
	}
	public ProductAttribute(String description) {
		super();
		//this.id = id;
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
