package com.saka.excelreader.models;

import java.util.LinkedHashMap;
import java.util.List;

public class UIProductByIdModel 
{
	private String name;
	private String description;
	private String imageURL;
	private LinkedHashMap<String, Object> productCategoryDetails;
	
	private List<UIProductModel> otherProductsFromSameCatagory;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public LinkedHashMap<String, Object> getProductCategory() {
		return productCategoryDetails;
	}

	public void setProductCategory(LinkedHashMap<String, Object> productCategoryDetails) {
		this.productCategoryDetails = productCategoryDetails;
	}

	public List<UIProductModel> getOtherProductsFromSameCatagory() {
		return otherProductsFromSameCatagory;
	}

	public void setOtherProductsFromSameCatagory(List<UIProductModel> otherProductsFromSameCatagory) {
		this.otherProductsFromSameCatagory = otherProductsFromSameCatagory;
	}
	
}
