package com.saka.excelreader.models;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="product_catagories")
public class ProductCategory {

	@Id
	@GeneratedValue
	private Long categoryId;

	private String catagoryName;

	@Column(columnDefinition = "TEXT")
	private String description;

	@ManyToMany
	@JoinTable(name = "catagory_features", 
				joinColumns = @JoinColumn(name = "catagory_ID"), 
				inverseJoinColumns = @JoinColumn(name = "feature_ID"))
	private List<Feature> features;

	@ManyToMany
	@JoinTable(name = "catagory_applications", joinColumns = @JoinColumn(name = "catagory_ID"), inverseJoinColumns = @JoinColumn(name = "application_ID"))
	private List<Application> applications;

	@OneToMany(mappedBy="productCategory")
	private List<Product> products;
	
	/*@OneToMany(mappedBy="productCategory")
	private List<Feature> features;
	
	@OneToMany(mappedBy="productCategory")
	private List<Application> applications;
	*/
	
	private ProductCategory() {
		super();
	}

	
	
	public ProductCategory(String catagoryName, String description) {
		super();
		this.catagoryName = catagoryName;
		this.description = description;
	}



	public ProductCategory(Long categoryId, String catagoryName, String description, List<Feature> features,
			List<Application> applications, List<Product> products) {
		super();
		this.categoryId = categoryId;
		this.catagoryName = catagoryName;
		this.description = description;
		this.features = features;
		this.applications = applications;
		this.products = products;
	}

	public Long getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Long categoryId) {
		this.categoryId = categoryId;
	}

	public String getCatagoryName() {
		return catagoryName;
	}

	public void setCatagoryName(String catagoryName) {
		this.catagoryName = catagoryName;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<Feature> getFeatures() {
		return features;
	}

	public void setFeatures(List<Feature> features) {
		this.features = features;
	}

	public List<Application> getApplications() {
		return applications;
	}

	public void setApplications(List<Application> applications) {
		this.applications = applications;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	

}
