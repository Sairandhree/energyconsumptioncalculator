package com.saka.excelreader.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="applications")
public class Application extends ProductAttribute
{
	/*@ManyToOne
	@JoinColumn(name="category_id")
	private ProductCategory productCategory ;
	*/
	
	@ManyToMany
	@JoinTable(name = "catagory_applications", 
			joinColumns = @JoinColumn(name = "application_ID"), 
			inverseJoinColumns = @JoinColumn(name = "catagory_ID"))
	private List<ProductCategory> productCategory;

	private Application() {
		super();
	}

	public Application( String description,List<ProductCategory> productCategory) 
	{
		super(description);
		this.productCategory=productCategory;
	}

	public List<ProductCategory> getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(List<ProductCategory> productCategory) {
		this.productCategory = productCategory;
	}
	
}
