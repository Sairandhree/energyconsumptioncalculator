package com.saka.excelreader.models;

public class InputModel {
	
	private double evapurationRate;
	private double inletTemperature;
	private double outletTemperature;
	
	private double solidContentInfieldLiquid;
	
	private double moistureInProduct;
	private double heatOfSolidFormation;
	private double feedTemperature;
	private double airHeatingEfficiency;
	private double fuelCalorificValue;
	public InputModel(double evapurationRate, double inletTemperature, double outletTemperature,
			double solidContentInfieldLiquid, double moistureInProduct, double heatOfSolidFormation,
			double feedTemperature, double airHeatingEfficiency, double fuelCalorificValue) {
		super();
		this.evapurationRate = evapurationRate;
		this.inletTemperature = inletTemperature;
		this.outletTemperature = outletTemperature;
		this.solidContentInfieldLiquid = solidContentInfieldLiquid;
		this.moistureInProduct = moistureInProduct;
		this.heatOfSolidFormation = heatOfSolidFormation;
		this.feedTemperature = feedTemperature;
		this.airHeatingEfficiency = airHeatingEfficiency;
		this.fuelCalorificValue = fuelCalorificValue;
	}
	public double getEvapurationRate() {
		return evapurationRate;
	}
	public void setEvapurationRate(double evapurationRate) {
		this.evapurationRate = evapurationRate;
	}
	public double getInletTemperature() {
		return inletTemperature;
	}
	public void setInletTemperature(double inletTemperature) {
		this.inletTemperature = inletTemperature;
	}
	public double getOutletTemprature() {
		return outletTemperature;
	}
	public void setOutletTemprature(double outletTemperature) {
		this.outletTemperature = outletTemperature;
	}
	public double getSolidContentInfieldLiquid() {
		return solidContentInfieldLiquid;
	}
	public void setSolidContentInfieldLiquid(double solidContentInfieldLiquid) {
		this.solidContentInfieldLiquid = solidContentInfieldLiquid;
	}
	public double getMoistureInProduct() {
		return moistureInProduct;
	}
	public void setMoistureInProduct(double moistureInProduct) {
		this.moistureInProduct = moistureInProduct;
	}
	public double getHeatOfSolidFormation() {
		return heatOfSolidFormation;
	}
	public void setHeatOfSolidFormation(double heatOfSolidFormation) {
		this.heatOfSolidFormation = heatOfSolidFormation;
	}
	public double getFeedTemperature() {
		return feedTemperature;
	}
	public void setFeedTemperature(double feedTemperature) {
		this.feedTemperature = feedTemperature;
	}
	public double getAirHeatingEfficiency() {
		return airHeatingEfficiency;
	}
	public void setAirHeatingEfficiency(double airHeatingEfficiency) {
		this.airHeatingEfficiency = airHeatingEfficiency;
	}
	public double getFuelCalorificValue() {
		return fuelCalorificValue;
	}
	public void setFuelCalorificValue(double fuelCalorificValue) {
		this.fuelCalorificValue = fuelCalorificValue;
	}
	
	
	
	

}
