package com.saka.excelreader.models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="features")
public class Feature extends ProductAttribute 
{
	
	/*@ManyToOne
	@JoinColumn(name="category_id")
	private ProductCategory productCategory ;
*/
	
	@ManyToMany
	@JoinTable(name = "catagory_features", 
				joinColumns = @JoinColumn(name = "feature_ID"), 
				inverseJoinColumns = @JoinColumn(name = "catagory_ID"))
	private List<ProductCategory> productCategory;
	
	private Feature() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Feature( String description,List<ProductCategory> productCategory) {
		super( description);
		this.productCategory = productCategory;
		// TODO Auto-generated constructor stub
	}

	public List<ProductCategory> getProductCategory() {
		return productCategory;
	}

	public void setProductCategory(List<ProductCategory> productCategory) {
		this.productCategory = productCategory;
	}

	
}
