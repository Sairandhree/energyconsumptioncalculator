package com.saka.excelreader.services;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.saka.excelreader.models.Application;
import com.saka.excelreader.models.ProductCategory;
import com.saka.excelreader.models.Feature;
import com.saka.excelreader.models.Product;
import com.saka.excelreader.repositories.ApplicationRepo;
import com.saka.excelreader.repositories.ProductCatagoryRepo;
import com.saka.excelreader.repositories.FeatureRepo;
import com.saka.excelreader.repositories.ProductRepo;

@Service
public class LoadDataService {

	private ProductRepo productRepo;
	private ProductCatagoryRepo productCatagoryRepo;
	private ApplicationRepo ApplicationRepo;
	private FeatureRepo FeatureRepo;

	@Autowired
	public LoadDataService(ProductRepo productRepo, ProductCatagoryRepo productCatagoryRepo,
			ApplicationRepo ApplicationRepo, FeatureRepo FeatureRepo) {
		super();
		this.productRepo = productRepo;
		this.productCatagoryRepo = productCatagoryRepo;
		this.ApplicationRepo = ApplicationRepo;
		this.FeatureRepo = FeatureRepo;
	}

	@PostConstruct
	private void loadData() {
		ProductCategory sprayDryer = createSprayDryer();
		List<ProductCategory> categoryList = new ArrayList<ProductCategory>();
		categoryList.add(sprayDryer);
		List <Application>  listApplications = loadApplications(categoryList);
		List <Feature>  listFeatures = loadFeatures(categoryList);
		createClosedCycleSprayDryer();
		createTwoStageSprayDryingSyste(sprayDryer);
		createDemoProduct(sprayDryer);
	
	}

	private void createDemoProduct(ProductCategory sprayDryer) 
	{
		Product p1 =new Product("automaticGrade1", "brand1", "capacity1", sprayDryer,"description1", "design1",
				"driven1", "material1", 100, "name1","url1");
		productRepo.save(p1);
		Product p2 =new Product("automaticGrade2", "brand2", "capacity2", sprayDryer,"description2", "design2",
				"driven2", "material2", 50, "name2","url2");
		productRepo.save(p2);
	}

	private void createTwoStageSprayDryingSyste(ProductCategory sprayDryer) {
		// TODO Auto-generated method stub
		
	}

	private void createClosedCycleSprayDryer() 
	{
		ProductCategory ClosedCycleSprayDryer = new ProductCategory ( "ClosedCycleSprayDryer","Closed Cycle Spray Dryer Description");
		productCatagoryRepo.save(ClosedCycleSprayDryer);
	}

	private List<Application> loadApplications(List<ProductCategory> sprayDryer) {
		List<Application> listApplication = new ArrayList<Application>();

		listApplication.add(new Application( "Chemical products",sprayDryer));
		listApplication.add(new Application( "Ceramic materials",sprayDryer));
		listApplication.add(new Application( "Detergents, soaps and surface active agents",sprayDryer));
		listApplication.add(new Application( "Pesticides, herbicides, fungicides, and insecticides",sprayDryer));
		listApplication.add(new Application( "Dyestuffs, pigments.",sprayDryer));
		listApplication.add(new Application( "Fertilizers",sprayDryer));
		listApplication.add(new Application( "CMineral flotation concentrates",sprayDryer));
		listApplication.add(new Application( "Inorganic and inorganic chemicals",sprayDryer));
		listApplication.add(new Application( "Food products",sprayDryer));
		return ApplicationRepo.saveAll(listApplication);

		

	}

	private List<Feature> loadFeatures(List<ProductCategory> sprayDryer) {
		List<Feature> listFeatures = new ArrayList<Feature>();
		listFeatures.add(new Feature(
				"Ability to operate in applications that range from aseptic pharmaceutical processing to ceramic powder production",sprayDryer));

		listFeatures.add(new Feature(
				"Can be designed to virtually any capacity required. Feed rates range from a few pounds per hour to over 30 tons per hour",sprayDryer));
		listFeatures
				.add(new Feature("Powder quality remains consistent during the entire run of the dryer",sprayDryer));
		listFeatures
				.add(new Feature( "Operation is continuous and adaptable to full automatic control.",sprayDryer));
		listFeatures.add(new Feature(
				"A great variety of spray dryer designs are available to meet various product specifications",sprayDryer));
		listFeatures
				.add(new Feature( "Can be used with both heat-resistant and heat sensitive products",sprayDryer));
		listFeatures.add(new Feature(
				"As long as they are can be pumped, the feedstock can be abrasive, corrosive, flammable, explosive or toxic",sprayDryer));
		listFeatures.add(
				new Feature( "Feedstock can be in solution, slurry, paste, gel, suspension or melt form",sprayDryer));
		listFeatures.add(new Feature( "Product density can be controlled",sprayDryer));
		listFeatures.add(new Feature( "Nearly spherical particles can be produced",sprayDryer));
		listFeatures.add(new Feature(
				"Material does not contact metal surfaces until dried, reducing corrosion problems",sprayDryer));
		return FeatureRepo.saveAll(listFeatures);

	

		// listFeatures.add(new Feature(1L, ""));

	}
	
	private ProductCategory createSprayDryer() {
		String sprayDryerDesc = "Spray drying is a method of producing a dry powder from a "
				+"liquid or slurry by rapidly drying with a hot gas. This is the preferred method of drying"
				+" of many thermally-sensitive materials such as foods and pharmaceuticals. "
				+"A consistent particle size distribution is a reason for spray drying some industrial "
				+"products such as catalysts. Air is the heated drying media. We offer a wide range of "
				+"Spray Drying Systems suited to continuous production of dry solids, granulate or agglomerate "
				+"particle form from liquid feedstock in one stage. Spray drying is one of the most widely"
				+"used industrial process for particle formation and drying.";
		//List<Product> products=new ArrayList<Product>();
		ProductCategory sprayDryer = new ProductCategory ( "Spray Dryer",sprayDryerDesc);
				
		productCatagoryRepo.save(sprayDryer);
		return sprayDryer;
	}

}
