package com.saka.excelreader.core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.hssf.record.crypto.Biff8EncryptionKey;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Component;

import com.saka.excelreader.models.InputModel;
import com.saka.excelreader.models.OutputModel;

@Component
public class ReadWrite {
	
	public void getData(){
		//ClassLoader classLoader = new ReadResourceFileDemo().getClass().getClassLoader();
		File  file = new File(getClass().getClassLoader().getResource("Test.xlsx").getFile());
		try {
			FileInputStream  fis = new FileInputStream(file);
			 XSSFWorkbook workbook = new XSSFWorkbook(fis);

	            XSSFSheet sheet = workbook.getSheetAt(0);
	            
	            FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
	           // workbook.getCreationHelper().createFormulaEvaluator().evaluateAll();
	           
	            XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);
	            
	            Cell cell1 = sheet.createRow(2).createCell(1);
	            cell1.setCellValue(1);
	            Cell cell2 = sheet.createRow(3).createCell(1);
	            cell2.setCellValue(2);
	            Cell cell3 = sheet.createRow(4).createCell(1);
	            cell3.setCellValue(3);
	            
	            //FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
	            //workbook.getCreationHelper().createFormulaEvaluator().evaluateAll();
	            
	            
	            XSSFSheet sheet2 = workbook.getSheetAt(1);
	            
	            Iterator<Row> rowIterator = sheet.iterator();
				while (rowIterator.hasNext()) 
				{
					Row row = rowIterator.next();
					//For each row, iterate through all the columns
					Iterator<Cell> cellIterator = row.cellIterator();
					
					while (cellIterator.hasNext()) 
					{
						Cell cell = cellIterator.next();
						//Check the cell type and format accordingly
						switch (cell.getCellType()) 
						{
							case NUMERIC:
								System.out.print(cell.getNumericCellValue() + "\t");
								break;
							case STRING:
								System.out.print(cell.getStringCellValue() + "\t");
								break;
						}
					}
					System.out.println("");
				}
				//file.close();
				
				
	          
            System.out.println(evaluator.evaluate(sheet2.getRow(1).getCell(1)).getNumberValue());
            System.out.println(evaluator.evaluate(sheet2.getRow(2).getCell(1)).getNumberValue());
            
            System.out.println(evaluator.evaluate(sheet2.getRow(3).getCell(1)).getNumberValue());
            
	            
		} catch (  IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	
	
	public OutputModel getOutput( InputModel input){
		//ClassLoader classLoader = new ReadResourceFileDemo().getClass().getClassLoader();
		
		OutputModel out = new OutputModel()	;
		try {
			 POIFSFileSystem  file = new POIFSFileSystem(new File(getClass().getClassLoader().getResource("working_srd_bf_2tph_product1.xls").getFile()),true);
			 Biff8EncryptionKey.setCurrentUserPassword("25783");
			//FileInputStream  fis = new FileInputStream(file);
			 HSSFWorkbook workbook = new HSSFWorkbook(file.getRoot(),true);
			
	            HSSFSheet inputSheet = workbook.getSheet("input");
	            
	            System.out.println(inputSheet.getRow(5).getCell(0));
	            
	            FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
	           // workbook.getCreationHelper().createFormulaEvaluator().evaluateAll();
	           
	            XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);
	            
	            Cell evaporationRate = inputSheet.createRow(1).createCell(2);
	            evaporationRate.setCellValue(input.getEvapurationRate());
	            
	            Cell inletTemp = inputSheet.createRow(2).createCell(2);
	            inletTemp.setCellValue(input.getInletTemperature());
	            
	            Cell outletTemp = inputSheet.createRow(3).createCell(2);
	            outletTemp.setCellValue(input.getOutletTemprature());
	            
	            Cell solidContentInfieldLiquid = inputSheet.createRow(4).createCell(2);
	            solidContentInfieldLiquid.setCellValue(input.getSolidContentInfieldLiquid());
	            
	            Cell moistureInProduct = inputSheet.createRow(5).createCell(2);
	            moistureInProduct.setCellValue(input.getMoistureInProduct());
	            
	            Cell heatSolid = inputSheet.createRow(6).createCell(2);
	            heatSolid.setCellValue(input.getHeatOfSolidFormation());
	            
	            Cell feedTemprature = inputSheet.createRow(7).createCell(2);
	            feedTemprature.setCellValue(input.getFeedTemperature());
	            
	            Cell airHeatingEfficiency = inputSheet.createRow(11).createCell(2);
	            airHeatingEfficiency.setCellValue(input.getAirHeatingEfficiency());
	            
	            Cell fuelCalorificValue = inputSheet.createRow(12).createCell(2);
	            fuelCalorificValue.setCellValue(input.getFuelCalorificValue());
	            
	           
	           
	            
	            
	            
	            
	            //FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
	           // workbook.getCreationHelper().createFormulaEvaluator().evaluateAll();
	            
	            HSSFSheet outputSheet = workbook.getSheet("output");
	         
	            	
	          out.setFuelCompsiontion(evaluator.evaluate(outputSheet.getRow(1).getCell(1)).getNumberValue());
	          out.setFuelCompsiontionPerKGEvaporation(evaluator.evaluate(outputSheet.getRow(2).getCell(1)).getNumberValue());
	          out.setFuelCompsiontionPerKGProduct(evaluator.evaluate(outputSheet.getRow(3).getCell(1)).getNumberValue());
	          
	          out.setFuelCost(evaluator.evaluate(outputSheet.getRow(4).getCell(1)).getNumberValue());
	          
	          out.setFuelCostPerKGEvaporation(evaluator.evaluate(outputSheet.getRow(5).getCell(1)).getNumberValue());
	          out.setFuelCostPerKGProduct(evaluator.evaluate(outputSheet.getRow(6).getCell(1)).getNumberValue());
	          
	          
	            
		} catch (  IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return out;
		
	}
	
	public static void main(String[] args) {
		ReadWrite rw = new ReadWrite();
		
		InputModel input = new InputModel(1000,250,100,42,2,217,25,0.85,8600);
		
		OutputModel output = rw.getOutput(input);
		
		System.out.println(output);
		
	}

}
